Some things to note:

1.The strings in AppManager and CoinFlip were not moved to strings.xml because they are used in public classes.
We have made attempts use getString() in order get the strings from strings.xml but this was not viable in all the solutions we have attempted.
All other strings that are are seen by the user have been moved to strings.xml however

2. If the user decides to change the speed of the timer, the user must select the a speed before choosing a duration or entering a duration.
If the user chooses a duration and then chooses a speed, a toast will appear to tell the user to reset the timer in order to set a speed.
If the user does not choose a speed and simply chooses a duration, the timer will countdown at 100% (Regular speed)
The speeds can be found at the top right corner on the toolbar (popdown menu)