package models;

/*
    Name:    Child.java
    Purpose: Stores the name and the path of the profile
    .        picture associated with a child. An instance
    .        of the child class is created when the user
    .        Adds a child in the configure my children
    .        feature.
*/
public class Child {
    private String name;
    private String path;

    public Child(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
