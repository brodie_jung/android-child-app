package models;

import java.util.ArrayList;
import java.util.LinkedList;

/*
    Name:    AppManager.java
    Purpose: Stores all the instances of the Child, CoinFlip and Task objects.
    .        Acts as a singleton where it passes the stored instances of
    .        Child, CoinFlip and Task between different activities such as
    .        the ChildrenActivity, FlipActivity and TaskListActivity.
*/
public class AppManager {

    private static final AppManager instance = new AppManager();
    private final LinkedList<Child> childrenList;
    private final ArrayList<CoinFlip> coinFlipsList;
    private final ArrayList<Task> taskList;
    private Child lastFlipped;

    private AppManager() {
        childrenList = new LinkedList<>();
        coinFlipsList = new ArrayList<>();
        taskList = new ArrayList<>();
    }

    public static AppManager getInstance() {
        return instance;
    }

    public void addChild(Child child) {
        childrenList.add(child);
        for (Task task : taskList) {
            task.updateListUponInsertion(child);
        }
    }

    public void deleteChild(int i) {
        Child removedChild = childrenList.remove(i);
        for (Task task : taskList) {
            task.updateListUponRemoval(removedChild);
        }
    }

    public Child getChild(int i) {
        return childrenList.get(i);
    }

    public Child getChild(String name) {
        for(Child child : childrenList) {
            if(child.getName().equals(name)) {
                return child;
            }
        }
        return null;
    }

    public void addCoinFlip(CoinFlip coinFlip) {
        coinFlipsList.add(coinFlip);
    }

    public ArrayList<CoinFlip> getChildCoinFlips(Child child) {
        ArrayList<CoinFlip> childCoinFlipList = new ArrayList<CoinFlip>();
        for(CoinFlip cf : coinFlipsList) {
            if(cf.getName().equals(child.getName())) {
                childCoinFlipList.add(cf);
            }
        }
        return childCoinFlipList;
    }

    public ArrayList<CoinFlip> getChildCoinFlips() {
        ArrayList<CoinFlip> childCoinFlipList = new ArrayList<CoinFlip>();
        for(CoinFlip cf : coinFlipsList) {
            if(cf.getName().equals("None")) {
                childCoinFlipList.add(cf);
            }
        }
        return childCoinFlipList;
    }

    public CoinFlip getCoinFlip(int i) {return coinFlipsList.get(i);}

    public ArrayList<CoinFlip> getCoinFlipList (){
        return coinFlipsList;
    }

    public int numberOfChildren() {
        return childrenList.size();
    }

    public int numberOfCoinFlips() {
        return coinFlipsList.size();
    }

    public void setLastFlipped(Child child) {
        lastFlipped = child;
    }

    public Child getLastFlipped() {
        return lastFlipped;
    }

    public void addTask(Task task) {
        taskList.add(task);
    }

    public void deleteTask(int i) {
        taskList.remove(i);
    }

    public void updateTasks(String existingName, Child editedChild) {
        for (Task task : taskList) {
            task.updateListUponEdit(existingName, editedChild);
        }
    }

    public void endTurn(Child child) {
        childrenList.remove(child);
        childrenList.add(child);
    }

    public String displayOverrideQueue(Child child) {
        StringBuilder queueStr = new StringBuilder();
        LinkedList<Child> tempList = new LinkedList<>(childrenList);
        tempList.remove(child);
        tempList.offerFirst(child);
        queueStr.append("Queue: ");
        for(Child c : tempList) {
            queueStr.append(c.getName()).append(" | ");
        }
        return queueStr.toString();
    }

    public Task getTask(int i) {
        return taskList.get(i);
    }

    public int numberOfTasks() {
        return taskList.size();
    }
}