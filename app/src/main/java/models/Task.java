package models;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

/*
    Name:       Task.java
    Purpose:    Stores the description and a queue
                of children in the order in which
                they should accomplish the task.
*/
public class Task {
    private String description;
    private final Queue<Child> turnQueue;

    public Task(String description) {
        this.description = description;
        turnQueue = new LinkedList<>();

        AppManager manager = AppManager.getInstance();
        List<Child> childList = new LinkedList();
        for (int i = 0; i < manager.numberOfChildren(); i++) {
            childList.add(manager.getChild(i));
        }

        // Put children from childList in turnQueue in random order
        Random randGenerator = new Random();
        while (!childList.isEmpty()) {
            int randPos = (int)(randGenerator.nextDouble() * childList.size());
            turnQueue.add(childList.remove(randPos));
        }
    }

    public void changeTaskName(String newName) {
        description = newName;
    }

    public String getDescription() { return description; }

    public boolean isQueueEmpty() { return turnQueue.isEmpty(); }

    public Child getNextInQueue() { return turnQueue.peek(); }

    public void confirmTurn() {
        if (isQueueEmpty()) {
            return;
        }
        turnQueue.add(turnQueue.remove());
    }

    // Handler method for when a child is deleted.
    public void updateListUponRemoval(Child removedChild) {
        for (int i = 0; i < turnQueue.size(); i++) {
            Child nonRemovedChild = turnQueue.remove();
            if (nonRemovedChild != removedChild) {
                turnQueue.add(nonRemovedChild);
            }
        }
    }

    // Handler method for when a child is edited.
    public void updateListUponEdit(String existingName, Child editedChild) {
        for (Child child : turnQueue) {
            if (child.getName().equals(existingName)) {
                child.setName(editedChild.getName());
                child.setPath(editedChild.getPath());
            }
        }
    }

    // Handler method for when a child is added.
    public void updateListUponInsertion(Child newChild) {
        turnQueue.add(newChild);
    }
}