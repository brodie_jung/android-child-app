package models;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
    Name:    CoinFlip.java
    Purpose: Stores the results of each coinflip.
    .        Allows each coinflip to be documented
    .        and associated to either a specific
    .        child or no child at all. The outcome
    .        of each coinflip is also stored and marked
    .        with the date/time of when it occurred.
*/
public class CoinFlip {

    private final String name;
    private final String choice;
    private final String result;
    long time;

    public CoinFlip(String name, String choice, String result, long time) {
        this.name = name;
        this.choice = choice;
        this.result = result;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public String print() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formattedTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(time);
        String outputTime = formattedTime.format(date);
        // Optimize with ternary operator
        return name.equals("None") ? result + " was flipped at " + outputTime
                                   : name + " Picked " + choice + " and got " + result + " at " + outputTime;
    }
}