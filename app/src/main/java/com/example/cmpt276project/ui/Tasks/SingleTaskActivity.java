package com.example.cmpt276project.ui.Tasks;

// Import classes
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;
import models.AppManager;
import models.Child;
import models.Task;

/*  The class for the activity of a single task.
    In this activity, the user should be allowed to have all
    settings and information related to one task in the list
    available. As such, the description of the task must be
    shown and the picture and name of the child who should
    do it next should be displayed. Furthermore, the user
    should be able to confirm the child's turn, start an
    activity to edit the task description, and delete the task.
 */
public class SingleTaskActivity extends AppCompatActivity {
    // Reference the singleton instance
    private AppManager manager = AppManager.getInstance();

    // View fields
    private TextView taskDescriptionTextView;
    private ImageView nextChildImageView;
    private TextView nextChildNameTextView;
    private Button confirmTurnButton;
    private Button editTaskButton;
    private Button deleteTaskButton;

    // State fields
    int taskPosition;
    Task currentTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_task);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        extractIntentData();
        setupViews();
    }

    private void setupViews() {
        // Get views
        taskDescriptionTextView = findViewById(R.id.display_task_description);
        nextChildImageView = findViewById(R.id.display_image_next_child);
        nextChildNameTextView = findViewById(R.id.display_text_next_child);
        confirmTurnButton = findViewById(R.id.button_confirm_turn);
        editTaskButton = findViewById(R.id.button_edit_task);
        deleteTaskButton = findViewById(R.id.button_delete_task);

        // Setup views
        setupTaskNameTextView();
        setupNextChildImageView();
        setupNextChildNameTextView();
        setupConfirmTurnButton();
        setupEditTaskButton();
        setupDeleteTaskButton();
    }

    private void setupTaskNameTextView() {
        taskDescriptionTextView.setText(currentTask.getDescription());
    }

    private void setupNextChildImageView() {
        if (currentTask.isQueueEmpty()) {
            nextChildImageView.setVisibility(View.INVISIBLE);
            return;
        }
        Child nextChild = currentTask.getNextInQueue();
        if (nextChild.getPath().equals(getString(R.string.empty_child_path))) {
            nextChildImageView.setImageResource(R.drawable.default_user);
        } else {
            setNextChildPicture(nextChild);
        }
    }

    private void setNextChildPicture(Child nextChild) {
        String pathString = nextChild.getPath();
        File imageFile = new File(pathString);
        Uri uriPath = Uri.fromFile(imageFile);
        InputStream inputStream;
        try {
            inputStream = getContentResolver().openInputStream(uriPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            nextChildImageView.setImageResource(R.drawable.default_user);
            return;
        }
        Bitmap imageBitmap = BitmapFactory.decodeStream(inputStream);
        nextChildImageView.setImageBitmap(imageBitmap);
    }

    private void setupNextChildNameTextView() {
        if (currentTask.isQueueEmpty()) {
            nextChildNameTextView.setVisibility(View.INVISIBLE);
            return;
        }
        nextChildNameTextView.setText(currentTask.getNextInQueue().getName());
    }

    private void setupConfirmTurnButton() {
        if (currentTask.isQueueEmpty()) {
            confirmTurnButton.setVisibility(View.INVISIBLE);
            return;
        }
        confirmTurnButton.setOnClickListener((v) -> {
            currentTask.confirmTurn();
            finish();
        });
    }

    private void setupEditTaskButton() {
        editTaskButton.setOnClickListener((v) -> {
            Intent editTaskActivityIntent = AddTaskActivity.makeIntent(this, taskPosition);
            startActivity(editTaskActivityIntent);
            finish();
        });
    }

    private void setupDeleteTaskButton() {
        deleteTaskButton.setOnClickListener((v) -> {
            manager.deleteTask(taskPosition);
            finish();
        });
    }

    private void extractIntentData() {
        Intent intent = getIntent();
        taskPosition = intent.getIntExtra("taskPosition", 0);
        currentTask = manager.getTask(taskPosition);
    }

    static public Intent makeIntent(Context context, int taskPosition) {
        // Error handling
        AppManager manager = AppManager.getInstance();
        if (context == null || taskPosition < 0 || taskPosition >= manager.numberOfTasks()) {
            throw new IllegalArgumentException("Invalid arguments.\n" +
                    "Context: " + context + "\n" +
                    "taskPosition: " + taskPosition);
        }

        Intent intent = new Intent(context, SingleTaskActivity.class);
        intent.putExtra("taskPosition", taskPosition);
        return intent;
    }
}