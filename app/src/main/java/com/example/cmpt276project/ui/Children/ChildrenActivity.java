package com.example.cmpt276project.ui.Children;

// Import classes
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;
import models.AppManager;
import models.Child;

/*
    Name:    ChildrenActivity.java
    Purpose: Displays a list of children. Children are listed
    .        in the order they were added. Clicking on a child
    .        will bring the user to the add children activity.
*/
public class ChildrenActivity extends AppCompatActivity {
    // Reference the singleton instance
    AppManager manager = AppManager.getInstance();

    // UI Widgets
    private TextView displayEmpty;
    private ListView childrenListView;
    private CustomAdaptor customAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.children_activity);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setupWidgets();
        setupAddChildButton();
        setupList();
        readFromFile();
    }

    private void setupWidgets() {
        displayEmpty = findViewById(R.id.text_empty_children);
        childrenListView = findViewById(R.id.list_children);
        customAdaptor = new CustomAdaptor();
    }

    /*
        Name:    onResume()
        Purpose: Displays the list of children upon reloading the
        .        ChildrenActivity.
    */
    @Override
    protected void onResume() {
        super.onResume();
        displayChildren();
        saveToFile();
    }

    /*
        Name:    setupAddChildButton()
        Purpose: Sets up the onClicklistener for adding a new child
    */
    private void setupAddChildButton() {
        Button btnAddChild = findViewById(R.id.button_add_child);
        btnAddChild.setOnClickListener(v -> {
            Intent intent  = AddChildrenActivity.makeIntent(ChildrenActivity.this);
            intent.putExtra("edit", false);
            startActivity(intent);
        });
    }

    /*
        Name:    setupList()
        Purpose: Sets up the onClickListener for the ListView
    */
    private void setupList() {
        childrenListView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = AddChildrenActivity.makeIntent(ChildrenActivity.this);
            intent.putExtra("edit", true);
            intent.putExtra("child", position);
            startActivity(intent);
        });
    }

    /*
        Name:    displayChildren()
        Purpose: Displays either a list of children
        .        or displays an empty screen.
    */
    private void displayChildren() {

        if (manager.numberOfChildren() == 0) {
            emptyChildrenList();
        } else {
            nonEmptyChildrenList();
        }
    }

    private void emptyChildrenList() {
        displayEmpty.setVisibility(View.VISIBLE);
        childrenListView.setVisibility(View.INVISIBLE);
        displayEmpty.setText(getString(R.string.empty_children_list));
    }

    private void nonEmptyChildrenList() {
        displayEmpty.setVisibility(View.INVISIBLE);
        childrenListView.setVisibility(View.VISIBLE);
        childrenListView.setAdapter(customAdaptor);
    }

    /*
        Name:    displayProfilePicture()
        Purpose: Displays the current configured
        .        profile picture of the child
    */
    private void displayProfilePicture(int childIndex, ImageView refImageView) {
        // Check if child already has a profile picture set
        if (!manager.getChild(childIndex).getPath().equals(getString(R.string.empty_path))) {
            // Display the custom set profile picture
            refImageView.setImageBitmap(setChildPhoto(Uri.fromFile(new File(manager.getChild(childIndex).getPath()))));
        } else {
            // Display default profile picture
            refImageView.setImageResource(R.drawable.default_user);
        }
    }

    /*
        Name:    setChildPhoto()
        Purpose: Creates a bitmap representation of a photo from
        .        a given file path.
    */
    private Bitmap setChildPhoto(Uri path) {
        try {
            InputStream inputStream = getContentResolver().openInputStream(path);
            return BitmapFactory.decodeStream(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveToFile() {
        SharedPreferences childPref = getSharedPreferences("PREF_CHILDREN",MODE_PRIVATE);
        SharedPreferences.Editor childEditor = childPref.edit();
        Gson gson = new Gson();
        int size = manager.numberOfChildren();
        for (int i = 0; i < size; i++) {
            String json = gson.toJson(manager.getChild(i));
            childEditor.putString("Child" + i, json);
        }
        childEditor.putInt("NumOfChildren", size);
        childEditor.apply();
    }

    private void readFromFile() {
        SharedPreferences myPref = getSharedPreferences("PREF_CHILDREN", MODE_PRIVATE);
        if (manager.numberOfChildren() == 0) {
            int size = myPref.getInt("NumOfChildren",0);
            Gson gson = new Gson();
            for(int i = 0; i < size; i++) {
                String json = myPref.getString("Child" + i, "");
                Child child = gson.fromJson(json, Child.class);
                manager.addChild(child);
            }
        }
    }

    public static Intent makeIntent(Context context) {
        return new Intent(context, ChildrenActivity.class);
    }

    /*
        Name:    CustomerAdaptor
        Extends: BaseAdapter
        Purpose: Modifies the BaseAdapter class to allow the
        .        ListView to display ImageViews and TextViews
        .        together.
    */
    private class CustomAdaptor extends BaseAdapter {
        @Override
        public int getCount() {
            return manager.numberOfChildren();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            @SuppressLint({"ViewHolder", "InflateParams"})
            View view = getLayoutInflater().inflate(R.layout.custom_listview, null);
            ImageView currImageView = (ImageView)view.findViewById(R.id.child_image_view);
            TextView currTextView = (TextView)view.findViewById(R.id.child_text_view);
            setTextViewProperties(currTextView);
            displayProfilePicture(position, currImageView);
            currTextView.setText(manager.getChild(position).getName());
            return view;
        }

        private void setTextViewProperties(TextView currTextView) {
            currTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        }
    }
}