package com.example.cmpt276project.ui.Tasks;

// Import classes
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import java.util.Objects;
import models.AppManager;
import models.Task;

/*
    Name:    AddTaskActivity.java
    Purpose: Allows the user to add or edit a task.
    .        User can change the description of a new
    .        or selected task
*/
public class AddTaskActivity extends AppCompatActivity {
    // Singleton field
    private AppManager manager = AppManager.getInstance();

    // View fields
    private TextView currentTaskNameTextView;
    private EditText taskNameEditText;
    private Button saveTaskButton;

    // State fields
    private boolean edit;
    private int taskPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_task);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        extractIntentData();
        configureViews();
    }

    // Configure views
    private void configureViews() {
        // Get views by id
        currentTaskNameTextView = findViewById(R.id.display_text_selected_task);
        taskNameEditText = findViewById(R.id.edit_text_description);
        saveTaskButton = findViewById(R.id.button_save_task);

        setupTaskNameTextView();
        setupSaveTaskButton();
    }

    private void setupSaveTaskButton() {
        int saveTaskButtonText;
        View.OnClickListener saveTaskButtonOnClickListener;

        if (edit) {
            saveTaskButtonText = R.string.button_edit_task;
            saveTaskButtonOnClickListener = (v) -> {
                String newTaskName = taskNameEditText.getText().toString();
                editTask(newTaskName);
            };
        } else {
            saveTaskButtonText = R.string.button_save_task;
            saveTaskButtonOnClickListener = (v) -> {
                String newTaskName = taskNameEditText.getText().toString();
                addTask(newTaskName);
            };
        }

        saveTaskButton.setText(saveTaskButtonText);
        saveTaskButton.setOnClickListener(saveTaskButtonOnClickListener);
    }

    private void setupTaskNameTextView() {
        if (edit) {
            Task currentTask = manager.getTask(taskPosition);
            currentTaskNameTextView.setText(currentTask.getDescription());
        }
    }

    private void editTask(String stringIn) {
        if (stringIn.equals(getString(R.string.invalid_input_add_task))) {
            Toast.makeText(this, getString(R.string.invalid_description_toast), Toast.LENGTH_SHORT).show();
        } else {
            Task task = manager.getTask(taskPosition);
            task.changeTaskName(stringIn);
            Toast.makeText(this, getString(R.string.toast_edited_task), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void addTask(String stringIn) {
        if (stringIn.equals(getString(R.string.invalid_input_add_task))) {
            Toast.makeText(this, getString(R.string.invalid_description_toast), Toast.LENGTH_SHORT).show();
        } else {
            Task newTask = new Task(stringIn);
            manager.addTask(newTask);
            Toast.makeText(this, getString(R.string.toast_added_task), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void extractIntentData() {
        Intent intent = getIntent();
        edit = intent.getBooleanExtra("edit", false);
        if (edit) {
            taskPosition = intent.getIntExtra("taskPosition", taskPosition);
            setTitle(getString(R.string.edit_task_title));
        } else {
            setTitle(getString(R.string.add_task_title));
        }
    }

    public static Intent makeIntent(Context context) {
        // Error handling
        if (context == null) {
            throw new IllegalArgumentException("Illegal argument.\n" +
                    "Context: " + context);
        }
        return new Intent(context, AddTaskActivity.class);
    }

    public static Intent makeIntent(Context context, int taskPosition) {
        // Error handling
        AppManager manager = AppManager.getInstance();
        if (context == null || taskPosition < 0 || taskPosition > manager.numberOfTasks()) {
            throw new IllegalArgumentException("Illegal argument.\n" +
                    "Context: " + context + "\n" +
                    "taskPosition: " + taskPosition);
        }

        Intent intent = new Intent(context, AddTaskActivity.class);
        intent.putExtra("edit", true);
        intent.putExtra("taskPosition", taskPosition);
        return intent;
    }
}