package com.example.cmpt276project.ui.Children;

// Import classes
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import models.AppManager;
import models.Child;

/*
    Name:    AddChildrenActivity.java
    Purpose: Allows user to configure their children.
    .        Allows the user to add, edit, and delete their
    .        children. Can also choose a photo from their phone
    .        or take a photo which will be used as a profile
    .        picture for each child.
*/
public class AddChildrenActivity extends AppCompatActivity {
    // Reference the singleton instance
    private final AppManager manager = AppManager.getInstance();
    private Child child;

    // Requests
    private static final int GALLERY_REQUEST = 9;
    private static final int CAMERA_REQUEST = 11;

    // Flags
    private boolean FLAG_PHOTO_MODIFIED;

    // UI Widgets
    private ImageView displayImageChild;
    private EditText childNameEditText;

    // Photo Path
    private Uri photoPath;

    // Global for Assigning Photo ID
    public static long PHOTO_ID_NUM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addchildren_activity);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        retrieveDataFromSharedPref();
        displayActivityState();
        setupActivityButtons();
    }

    private void displayActivityState() {
        setupChildProfile();
        if (getIntent().getBooleanExtra("edit", false)) {
            setTitle(getString(R.string.title_edit_children));
            displayCurrentChild();
            displayEditSelectedImage();
        } else {
            setTitle(getString(R.string.title_add_children));
            displayAddSelectedImage();
        }
    }

    private void setupChildProfile() {
        displayImageChild = (ImageView)findViewById(R.id.display_image_child);
        childNameEditText = (EditText)findViewById(R.id.edit_text_child_name);
        FLAG_PHOTO_MODIFIED = false;
    }

    /*
        Name:    displayCurrentChild()
        Purpose: Displays the current selected child
        .        into the display text TextView
    */
    @SuppressLint("SetTextI18n")
    private void displayCurrentChild() {
        TextView displayNameChild = (TextView)findViewById(R.id.display_text_child);
        child = manager.getChild(getIntent().getIntExtra("child", 0));
        childNameEditText.setText(child.getName());
        displayNameChild.setText(getString(R.string.selected_child, child.getName()));
    }

    /*
        Name:    displayEditSelectedImage()
        Purpose: Displays the current configured
        .        profile picture of the child
    */
    private void displayEditSelectedImage() {
        // Check if child already has a profile picture set
        if (!child.getPath().equals(getString(R.string.empty_path))) {
            // Display the custom set profile picture
            displayImageChild.setImageBitmap(setChildPhoto(Uri.fromFile(new File(child.getPath()))));
        } else {
            // Display default profile picture
            displayImageChild.setImageResource(R.drawable.default_user);
        }
    }

    private void displayAddSelectedImage() {
        // Display default profile picture
        displayImageChild.setImageResource(R.drawable.default_user);
    }

    /*
        Name:    setupActivityButtons()
        Purpose: Sets up button listeners.
    */
    private void setupActivityButtons() {
        setupSaveButton();
        setupDeleteButton();
        setupSelectPhotoButton();
    }

    private void setupSaveButton() {
        // Save
        Button buttonSaveChild = findViewById(R.id.button_save_child);
        buttonSaveChild.setOnClickListener(v -> {
            if (getIntent().getBooleanExtra("edit", false)) {
                editChild();
            } else {
                addChild();
            }
        });
    }

    private void setupDeleteButton() {
        // Delete
        Button buttonDeleteChild = findViewById(R.id.button_delete_child);
        buttonDeleteChild.setOnClickListener(v -> deleteChild());
        if (!getIntent().getBooleanExtra("edit", false)) {
            buttonDeleteChild.setVisibility(View.INVISIBLE);
        } else {
            buttonDeleteChild.setVisibility(View.VISIBLE);
        }
    }

    private void setupSelectPhotoButton() {
        // Select Photo
        Button buttonSelectPhoto = findViewById(R.id.button_select_photo);
        buttonSelectPhoto.setOnClickListener(v -> displayImageSelectionOptions());
    }

    /*
        Name:    addChild()
        Purpose: Creates a new child and appends it to
        .        the arraylist of children in the singleton.
    */
    private void addChild() {
        String name = childNameEditText.getText().toString();
        if (name.equals(getString(R.string.invalid_input_add_children))) {
            Toast.makeText(this, getString(R.string.invalid_input_toast), Toast.LENGTH_SHORT).show();
        } else {
            manager.addChild(
                FLAG_PHOTO_MODIFIED ?                                                           // Checks if the user has set a profile picture for the child
                    new Child(name, photoPath.toString()) :                                     // Will store the path of the current set profile photo
                    new Child(name, getString(R.string.empty_path))                             // Will have no path assigned
            );
            Toast.makeText(this, getString(R.string.toast_added_child), Toast.LENGTH_SHORT).show();
            saveDataIntoSharedPref();                                                           // Save the Photo ID constant into shared prefs
            finish();
        }
    }

    /*
        Name:    editChild()
        Purpose: Edits the name and profile picture of a
        .        selected child.
    */
    private void editChild() {
        Intent intent = getIntent();
        Child child = manager.getChild(intent.getIntExtra("child", 0));
        String name = childNameEditText.getText().toString();
        if (name.equals(getString(R.string.invalid_input_add_children))) {
            Toast.makeText(this, getString(R.string.invalid_input_toast), Toast.LENGTH_SHORT).show();
        } else {
            String existingName = child.getName();
            child.setName(name);
            child.setPath(FLAG_PHOTO_MODIFIED ?                                                 // Checks if the user has set a profile picture for the child
                    photoPath.toString() :                                                      // Will store the path of the current set profile photo
                    child.getPath()                                                             // Will retain the unchanged path
            );
            manager.updateTasks(existingName, child);                                           // Update each child in every task's queue
            Toast.makeText(this, getString(R.string.toast_edited_child), Toast.LENGTH_SHORT).show();
            saveDataIntoSharedPref();                                                           // Save the Photo ID constant into shared prefs
            finish();
        }
    }

    /*
        Name:    deleteChild()
        Purpose: Deletes the current selected child
    */
    private void deleteChild() {
        if (manager.numberOfChildren() > 0) {
            manager.deleteChild(getIntent().getIntExtra("child", 0));
            Toast.makeText(this, getString(R.string.toast_deleted_child), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.toast_empty_children), Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    /*
        Name:    displayImageSelectionOptions()
        Purpose: Displays an alert dialog to allow the
        .        user to either choose a photo from the
        .        gallery or take a photo with the device's
        .        camera
    */
    private void displayImageSelectionOptions() {
        final String[] selectionOptions = getResources().getStringArray(R.array.photo_options);
        AlertDialog.Builder selectionBuilder = new AlertDialog.Builder(this);
        selectionBuilder.setTitle(R.string.title_select_photo_alert_dialog)                                        // Will display alert dialog with two options
                        .setItems(selectionOptions, (dialog, whichOption) ->
                                photoSelectionOptions(whichOption));
        AlertDialog photoDialog = selectionBuilder.create();
        photoDialog.show();
    }

    private void photoSelectionOptions(int selectedOption) {
        switch (selectedOption) {
            case 0:
                fetchGalleryPhoto();                                                            // Choose photo from gallery
                break;
            case 1:
                fetchCameraPhoto();                                                             // Take photo with camera
                break;
        }
    }

    private void fetchGalleryPhoto() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GALLERY_REQUEST);
    }

    private void fetchCameraPhoto() {
        Intent cameraIntent = new Intent();
        cameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    /*
        Name:    onActivityResult()
        Purpose: Handles requests for either getting a photo from
        .        the gallery or from taking a photo with a camera.
        .        Will save the Uri (file path) and the photo.
    */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {        // Choose photo from gallery
            Bitmap bitmap = setChildPhoto(data.getData());
            displayImageChild.setImageBitmap(bitmap);
            assert bitmap != null;
            photoPath = savePhoto(this, bitmap);
        } else if(requestCode == CAMERA_REQUEST && resultCode == RESULT_OK && data != null) {   // Take photo with camera
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            displayImageChild.setImageBitmap(bitmap);
            photoPath = savePhoto(this, bitmap);
        }
        FLAG_PHOTO_MODIFIED = true;
    }

    /*
        Name:    savePhoto()
        Purpose: Saves a photo to the external storage of the device.
        .        Assigns each photo a unique ID. Also returns the path
        .        to the location of the saved photo.
    */
    private Uri savePhoto(Context context, Bitmap bitmap) {
        @SuppressLint("SimpleDateFormat")
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        ContextWrapper wrapper = new ContextWrapper(context);
        File createFile = wrapper.getDir("Images", MODE_PRIVATE);
        createFile = new File(createFile, "snap_" + timeStamp + "_" + PHOTO_ID_NUM + ".jpg");
        try {
            OutputStream outputStream = new FileOutputStream(createFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
            PHOTO_ID_NUM++;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Uri.parse(createFile.getAbsolutePath());
    }

    /*
        Name:    setChildPhoto()
        Purpose: Creates a bitmap representation of a photo from
        .        a given file path.
    */
    private Bitmap setChildPhoto(Uri path) {
        try {
            InputStream inputStream = getContentResolver().openInputStream(path);
            return BitmapFactory.decodeStream(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveDataIntoSharedPref() {
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("photoIDNum", PHOTO_ID_NUM);
        editor.apply();
    }

    private void retrieveDataFromSharedPref() {
        // defValue means default value
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        PHOTO_ID_NUM = prefs.getLong("photoIDNum", 1);
    }

    public static Intent makeIntent(Context context) {
        return new Intent(context, AddChildrenActivity.class);
    }
}