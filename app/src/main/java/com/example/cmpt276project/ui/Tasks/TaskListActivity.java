package com.example.cmpt276project.ui.Tasks;

// Import classes
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.example.cmpt276project.R;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;
import models.AppManager;
import models.Child;
import models.Task;

/*
    Name:    TaskListActivity.java
    Purpose: Displays a list of tasks. Tasks are listed in the
    .        they were added. Clicking on a task will bring the
    .        user to the SingleTaskActivity where the user can
    .        see the selected task in detail.
*/
public class TaskListActivity extends AppCompatActivity {
    // Reference the singleton instance
    AppManager manager = AppManager.getInstance();

    // UI Widgets
    private TextView displayEmpty;
    private ListView taskListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setupWidgets();
        setupAddTaskButton();
        setupList();
        retrieveDataFromSharedPref();
    }

    private void setupWidgets() {
        displayEmpty = findViewById(R.id.text_empty_tasks);
        taskListView = findViewById(R.id.list_tasks);
    }

    /*
        Name:    onResume()
        Purpose: Displays the list of tasks upon reloading the
        .        TaskListActivity.
    */
    @Override
    protected void onResume() {
        super.onResume();
        displayTasks();
        saveDataIntoSharedPref();
    }

    /*
        Name:    setupAddTaskButton()
        Purpose: Sets up the onClicklistener for adding a new task
    */
    private void setupAddTaskButton() {
        Button btnAddTask = findViewById(R.id.button_add_task);
        btnAddTask.setOnClickListener(v -> {
            Intent intent  = AddTaskActivity.makeIntent(TaskListActivity.this);
            intent.putExtra("edit", false);
            startActivity(intent);
        });
    }

    /*
        Name:    setupList()
        Purpose: Sets up the onClickListener for the ListView
    */
    private void setupList() {
        taskListView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = SingleTaskActivity.makeIntent(TaskListActivity.this, position);
            intent.putExtra("task", position);
            startActivity(intent);
        });
    }

    /*
        Name:    displayTasks()
        Purpose: Displays either a list of tasks
        .        or displays an empty screen.
    */
    private void displayTasks() {

        if (manager.numberOfTasks() == 0) {
            emptyTaskList();
        } else {
            nonEmptyTaskList();
        }
    }

    private void emptyTaskList() {
        displayEmpty.setVisibility(View.VISIBLE);
        taskListView.setVisibility(View.INVISIBLE);
        displayEmpty.setText(getString(R.string.empty_task_list));
    }

    private void nonEmptyTaskList() {
        displayEmpty.setVisibility(View.INVISIBLE);
        taskListView.setVisibility(View.VISIBLE);
        setTaskListLayout();
    }

    /*
        Name:    setTaskListLayout()
        Purpose: Retrieves the task description and
        .        the turn of the child from every Task
        .        object stored in the singleton. Then it
        .        will display each task in as a TextView
        .        on the UI.
    */
    private void setTaskListLayout() {
        TaskArrayAdapter adapter = new TaskArrayAdapter();
        taskListView.setAdapter(adapter);
        for (int i = 0; i < manager.numberOfTasks(); i++) {
            adapter.add(manager.getTask(i));
        }
    }

    private void saveDataIntoSharedPref() {
        SharedPreferences prefs = getSharedPreferences("PREF_TASKS", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        int size = manager.numberOfTasks();
        for (int i = 0; i < size; i++) {
            String json = gson.toJson(manager.getTask(i));
            editor.putString("Task" + i, json);
        }
        editor.putInt("NumOfTasks", size);
        editor.apply();
    }

    private void retrieveDataFromSharedPref() {
        retrieveChildrenFromSharedPref();
        retrieveTasksFromSharedPref();
    }

    private void retrieveChildrenFromSharedPref() {
        SharedPreferences childPreferences = getSharedPreferences("PREF_CHILDREN", MODE_PRIVATE);
        if (manager.numberOfChildren() == 0) {
            int size = childPreferences.getInt("NumOfChildren", 0);
            Gson gson = new Gson();
            for (int i = 0; i < size; i++) {
                String json = childPreferences.getString("Child" + i, "");
                Child child = gson.fromJson(json, Child.class);
                manager.addChild(child);
            }
        }
    }

    private void retrieveTasksFromSharedPref() {
        SharedPreferences taskPreferences = getSharedPreferences("PREF_TASKS", MODE_PRIVATE);
        if (manager.numberOfTasks() == 0) {
            int size = taskPreferences.getInt("NumOfTasks", 0);
            Gson gson = new Gson();
            for (int i = 0; i < size; i++) {
                String json = taskPreferences.getString("Task" + i, "");
                Task task = gson.fromJson(json, Task.class);
                manager.addTask(task);
            }
        }
    }

    public static Intent makeIntent(Context context) {
        return new Intent(context, TaskListActivity.class);
    }

    private class TaskArrayAdapter extends ArrayAdapter<Task> {
        public TaskArrayAdapter() {
            super(TaskListActivity.this, R.layout.task_entry);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.task_entry, parent, false);
            }

            // Get views
            ImageView thumbnail = itemView.findViewById(R.id.task_entry_thumbnail);
            TextView text = itemView.findViewById(R.id.task_entry_description);

            Task task = manager.getTask(position);

            if (task.isQueueEmpty()) {
                thumbnail.setImageResource(R.drawable.default_user);
            } else {
                try {
                    Bitmap thumbnailBitmap = getThumbnailBitmap(task);
                    thumbnail.setImageBitmap(thumbnailBitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    thumbnail.setImageResource(R.drawable.default_user);
                }
            }

            String textString = getString(R.string.task_string_1, task.getDescription());
            if (!task.isQueueEmpty()) {
                textString = getString(R.string.task_string_2, textString, task.getNextInQueue().getName());
            }
            text.setText(textString);
            return itemView;
        }

        // Precondition: Task queue must NOT be empty.
        private Bitmap getThumbnailBitmap(Task task) throws FileNotFoundException {
            Child nextChild = task.getNextInQueue();
            File imageFile = new File(nextChild.getPath());
            Uri uriPath = Uri.fromFile(imageFile);
            InputStream inputStream = getContentResolver().openInputStream(uriPath);
            return BitmapFactory.decodeStream(inputStream);
        }
    }
}