package com.example.cmpt276project.ui.CoinFlip;

// Import classes
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Objects;
import models.AppManager;
import models.Child;
import models.CoinFlip;

/*
    Name:    FlipSelectionActivity.java
    Purpose: Allows the user to pick a child to toss the coin.
    .        This class includes the menu to pick a child and
    .        have the coin toss be associated with the chosen
    .        child, or skip straight to the coin toss activity.
*/
public class FlipSelectionActivity extends AppCompatActivity {
    // Reference the singleton instance
    private final AppManager manager = AppManager.getInstance();

    // References to selected child and choice
    private String choosingChild;
    private String choice;
    private Child childLastFlipped;
    private final ArrayList<String> childrenNames = new ArrayList<>();

    // Flags
    private Boolean override = false;

    // UI Widgets
    private Spinner childNamesSpinner;
    private Spinner choicesSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flip_selection);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        retrieveDataFromSharedPref();
        setupWidgets();
        setupList();
        setupButtons();
        if (manager.numberOfChildren() < 1) {
            Intent intent = FlipActivity.makeIntent(FlipSelectionActivity.this);
            intent.putExtra("Skip", true);
            startActivity(intent);
        }
    }

    /*
        Name:    onResume()
        Purpose: Displays the list of children and choices
        .        upon reloading the FlipSelectionActivity.
    */
    @Override
    protected void onResume() {
        super.onResume();
        displayDropDownList();
        if (manager.numberOfChildren() < 1) {
            finish();
        }
    }

    private void setupWidgets() {
        childNamesSpinner = findViewById(R.id.child1_spinner);
        choicesSpinner = findViewById(R.id.flip_spinner);
    }

    private void setupButtons() {
        setupReturnButton();
        setupSkipButton();
        setupHistoryButton();
    }

    /*
        Name:    setupHistoryButton()
        Purpose: Sets up the onClicklistener for seeing history of
        .        coinflips.
    */
    private void setupHistoryButton() {
        Button btnHistory = findViewById(R.id.fliphistoryBtn);
        btnHistory.setOnClickListener(v -> {
            Intent intent = FlipHistoryActivity.makeIntent(FlipSelectionActivity.this);
            startActivity(intent);
        });
    }

    /*
        Name:    setupReturnButton()
        Purpose: Sets up the onClicklistener for saving a coinflip
        .        with a child. (Associated with a child)
    */
    private void setupReturnButton() {
        Button buttonConfirm = findViewById(R.id.button_return);
        buttonConfirm.setOnClickListener(v -> {
            if (!override) {
                childLastFlipped = manager.getChild(choosingChild);
                manager.setLastFlipped(childLastFlipped);
                manager.endTurn(childLastFlipped);
                Intent intent = FlipActivity.makeIntent(FlipSelectionActivity.this);
                intent.putExtra("Selection", choice);
                startActivity(intent);
                saveDataIntoSharedPref();
            } else {
                Intent intent = FlipActivity.makeIntent(FlipSelectionActivity.this);
                intent.putExtra("Skip", true);
                startActivity(intent);
            }
        });
    }

    /*
        Name:    setupSkipButton()
        Purpose: Sets up the onClicklistener for just flipping a
        .        coin. (No association to any child)
    */
    private void setupSkipButton() {
        Button btnSkip = findViewById(R.id.button_skip);
        btnSkip.setOnClickListener(v -> {
            Intent intent = FlipActivity.makeIntent(FlipSelectionActivity.this);
            intent.putExtra("Skip", true);
            startActivity(intent);
        });
    }

    /*
        Name:    displayDropDownList()
        Purpose: Displays the spinner for child and coinflip.
    */
    private void displayDropDownList() {
        childLastFlipped = manager.getLastFlipped();
        for (int i = 0; i < manager.numberOfChildren(); i++) {
            Child child = manager.getChild(i);
            childrenNames.add(child.getName());
        }
        childrenNames.add(getString(R.string.choice_none));

        childNamesSpinner.setAdapter(new CustomDropDownAdapter(childrenNames));
        choicesSpinner.setAdapter(new ArrayAdapter<String> (
                this,
                R.layout.child_dropdown_list,
                getResources().getStringArray(R.array.child_choices)
        ));
    }

    private void setupList() {
        setupChildrenList();
        setupChoicesList();
    }

    private void setupChildrenList() {
        childNamesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ImageView childPicture = findViewById(R.id.child_picture_coinflip);
                if (position == manager.numberOfChildren()) {
                    override = true;
                    childPicture.setImageResource(R.drawable.default_user);
                } else {
                    displayProfilePicture(position, childPicture);
                }
                choosingChild = childrenNames.get(position);
                whoGoesFirst();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void setupChoicesList() {
        choicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    choice = getString(R.string.flip_result_heads);
                } else {
                    choice = getString(R.string.flip_result_tails);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    @SuppressLint("SetTextI18n")
    private void whoGoesFirst() {
        TextView txtSelection = findViewById(R.id.text_selection);
        TextView queue = findViewById(R.id.txt_display_queue);
        Button skipButton = findViewById(R.id.button_skip);
        if (choosingChild.equals(getString(R.string.choice_none))) {
            txtSelection.setText(getString(R.string.choice_override));
            queue.setText(getString(R.string.choice_empty));
            skipButton.setVisibility((View.GONE));
        } else {
            txtSelection.setText(getString(R.string.child_chooses_option, choosingChild));
            queue.setText(manager.displayOverrideQueue(manager.getChild(choosingChild)));
            skipButton.setVisibility((View.VISIBLE));
        }
    }

    private void saveDataIntoSharedPref() {
        SharedPreferences childPref = getSharedPreferences("PREF_CHILDREN", MODE_PRIVATE);
        SharedPreferences.Editor childEditor = childPref.edit();
        Gson gson = new Gson();
        int size = manager.numberOfChildren();
        for (int i = 0; i < size; i++) {
            String json = gson.toJson(manager.getChild(i));
            childEditor.putString("Child" + i, json);
        }
        childEditor.putInt("NumOfChildren", size);
        childEditor.apply();
    }

    private void retrieveDataFromSharedPref() {
        retrieveChildrenFromSharedPref();
        retrieveCoinFlipsFromSharedPref();
    }

    private void retrieveChildrenFromSharedPref() {
        SharedPreferences childPreferences = getSharedPreferences("PREF_CHILDREN", MODE_PRIVATE);
        if (manager.numberOfChildren() == 0) {
            int size = childPreferences.getInt("NumOfChildren",0);
            Gson gson = new Gson();
            for (int i = 0; i < size; i++) {
                String json = childPreferences.getString("Child" + i, "");
                Child child = gson.fromJson(json, Child.class);
                manager.addChild(child);
            }
        }
    }

    private void retrieveCoinFlipsFromSharedPref() {
        SharedPreferences flipPreferences = getSharedPreferences("PREF_FLIP", MODE_PRIVATE);
        if (manager.numberOfCoinFlips() == 0) {
            int size = flipPreferences.getInt("NumOfCoinFlip",0);
            Gson gson = new Gson();
            for (int i = 0; i < size; i++) {
                String json = flipPreferences.getString("CoinFlip" + i, "");
                CoinFlip coinFlip = gson.fromJson(json, CoinFlip.class);
                manager.addCoinFlip(coinFlip);
            }
            String json = flipPreferences.getString("LastFlipped", "");
            Child lastFlipped = gson.fromJson(json, Child.class);
            manager.setLastFlipped(lastFlipped);
        }
    }

    /*
        Name:    displayProfilePicture()
        Purpose: Displays the current configured
        .        profile picture of the child
    */
    private void displayProfilePicture(int childIndex, ImageView refImageView) {
        // Check if child already has a profile picture set
        if (!manager.getChild(childIndex).getPath().equals(getString(R.string.empty_path))) {
            // Display the custom set profile picture
            refImageView.setImageBitmap(setChildPhoto(Uri.fromFile(new File(manager.getChild(childIndex).getPath()))));
        } else {
            // Display default profile picture
            refImageView.setImageResource(R.drawable.default_user);
        }
    }

    /*
        Name:    setChildPhoto()
        Purpose: Creates a bitmap representation of a photo from
        .        a given file path.
    */
    private Bitmap setChildPhoto(Uri path) {
        try {
            InputStream inputStream = getContentResolver().openInputStream(path);
            return BitmapFactory.decodeStream(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Intent makeIntent(Context context) {
        return new Intent(context, FlipSelectionActivity.class);
    }

    /*
        Name:    CustomerDropdownAdaptor
        Extends: BaseAdapter
        Purpose: Modifies the BaseAdapter class to allow the
        .        spinner to display ImageViews and TextViews
        .        together.
    */
    private class CustomDropDownAdapter extends BaseAdapter {
        private final ArrayList<String> childrenNames;

        public CustomDropDownAdapter(ArrayList<String> childrenNames) {
            this.childrenNames = childrenNames;
        }

        @Override
        public int getCount() {
            return manager.numberOfChildren() + 1;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            @SuppressLint({"ViewHolder", "InflateParams"})
            View view = getLayoutInflater().inflate(R.layout.custom_dropdown_spinner, null);
            ImageView currImageView = (ImageView)view.findViewById(R.id.child_dropdown_image_view);
            TextView currTextView = (TextView)view.findViewById(R.id.child_dropdown_text_view);
            setTextViewProperties(currTextView, childrenNames, position);
            setImageViewProperties(currImageView, position);
            return view;
        }

        private void setImageViewProperties(ImageView currImageView, int position) {
            if (position == manager.numberOfChildren()) {
                currImageView.setImageResource(R.drawable.default_user);
            } else {
                displayProfilePicture(position, currImageView);
            }
        }

        private void setTextViewProperties(TextView currTextView, ArrayList<String> childrenNames, int position) {
            currTextView.setText(childrenNames.get(position));
            currTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        }
    }
}