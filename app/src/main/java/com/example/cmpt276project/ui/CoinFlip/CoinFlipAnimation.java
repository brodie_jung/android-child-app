package com.example.cmpt276project.ui.CoinFlip;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.widget.ImageView;
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;

/*  The CoinFlipAnimation is a separate class which is solely focused in the execution of the coin
    flipping animation in the flip activity. This class can, in theory, animate any ImageView object
    and perform an animation similar to spinning a coin as in a toss, but such use is not recommended.
 */
public class CoinFlipAnimation {

    // Animation object fields
    private final ImageView coin;                               // ImageView coin object
    private ObjectAnimator shrinkX;                             // Animation responsible for decreasing x scale values
    private ObjectAnimator expandX;                             // Animation responsible for increasing x scale values
    private AnimatorSet fullRotation;                           // Animation that comprehends one full spin

    // Animation value fields
    private double rps;                                         // Rotations per second at a current animation instance
    private long rotationTimeElapsed;                           // Time elapsed since beginning of animation

    // Final animation value fields
    private final long TIME = 3000;                             // The total time for the animation to complete (changing it might cause it to break)
    private final double INITIAL_RPS = 20;                      // Initial rotations per second (altering might compromise animation quality)
    private final double SECOND = 1000;                         // Time of milliseconds in a second

    // Others
    private final List<Runnable> listeners = new ArrayList<>();
    private boolean animationDisabled = false;

    public CoinFlipAnimation(ImageView coin) {
        this.coin = coin;

    }

    // Creates a new instance of an animation and starts it, unless animation is running or is disabled.
    public void startAnimation() {
        if (animationDisabled || (fullRotation != null && fullRotation.isRunning())) {
            return;
        }

        shrinkX = ObjectAnimator.ofFloat(coin, "scaleX", 1f, 0f);
        expandX = ObjectAnimator.ofFloat(coin, "scaleX", 0f, 1f);
        fullRotation = new AnimatorSet();
        fullRotation.play(shrinkX).before(expandX);
        fullRotation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) { }
            @Override
            public void onAnimationEnd(Animator animation) {
                rotateAnimation();
            }
            @Override
            public void onAnimationCancel(Animator animation) { }
            @Override
            public void onAnimationRepeat(Animator animation) { }
        });
        this.rotationTimeElapsed = 0;
        this.rps = INITIAL_RPS;
        rotateAnimation();
    }

    public void disableAnimation() {
        animationDisabled = true;
    }

    private void callListeners() {
        for (Runnable function : listeners) {
            function.run();
        }
    }

    /*  The rotateAnimation() method is the method responsible executing the animation. It will
        calculate the rotations per second with the function calcRps(), configure the animation so
        that it will rotate at the RPS speed, execute the animation and update the time.

        Although each call to this method only counts as one full spin, in startAnimation() method,
        rotateAnimation() was configured to be called whenever the animation ends, thus causing a
        feedback loop. This feedback helps determine the appropriate RPS for each spin, and also
        determines whether another spin should happen or not.
     */
    private void rotateAnimation() {
        if (rotationTimeElapsed >= TIME) {
            callListeners();
            return;
        }
        rps = calcRps();
        long duration = (long)(SECOND / rps);
        rotationDuration(duration);
        fullRotation.start();
        rotationTimeElapsed += duration;
    }

    // Sets the time total spin time of the coin to duration.
    private void rotationDuration(long duration) {
        shrinkX.setDuration(duration / 2);
        expandX.setDuration(duration / 2);
    }

    /*  Outputs the RPS in accordance to rotationElapsedTime.
        The function is a piece-wise function where, for all elapsed time less than 1s, it will
        return the RPS in INITIAL_RPS, and for any greater amount of time, it will calculate the
        RPS according to a square root formula. This formula parameters are set up specifically
        for the current values of TIME and INITIAL_RPS, such at when rotationTimeElapsed is greater
        than, but still close to 1s, the output will be close to INITIAL_RPS, so to simulate a smooth
        deceleration.

        As such, either altering the parameters of this method, of TIME, or of INITIAL_RPS may either
        compromise the quality of the animation (by causing a more abrupt or even "nonsensical"
        deceleration) or even break it (if the function returns a negative value).

        In summary, this is not a very flexible animation as it does not allow for many different
        values without compromising the animation, but it works well for the current values.
     */
    private double calcRps() {
        if (TIME - rotationTimeElapsed <= 1000) {
            final double x = rotationTimeElapsed / 1000.0;
            final double a = 0.00005;
            final double b = 0;
            final double c = 81.6;

            return (Math.pow(a * x + b, -0.5) - c);
        } else {
            return INITIAL_RPS;
        }
    }

    public void addListener(Runnable function) {
        listeners.add(function);
    }
}