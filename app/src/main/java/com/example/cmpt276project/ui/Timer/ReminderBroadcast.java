package com.example.cmpt276project.ui.Timer;

// Import classes
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.example.cmpt276project.R;

/*
    Name:    ReminderBroadcast.java
    Purpose: Creates and initializes the contents
    .        of the notification that will alert
    .        the user when the timer finishes
*/
public class ReminderBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent returnIntent = new Intent(context, TimerActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                context,
                1,
                returnIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "notifyTimer")
                .setSmallIcon(R.drawable.ic_bell)
                .setContentTitle(context.getString(R.string.notification_title))
                .setContentText(context.getString(R.string.notification_text))
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(200, builder.build());
    }
}