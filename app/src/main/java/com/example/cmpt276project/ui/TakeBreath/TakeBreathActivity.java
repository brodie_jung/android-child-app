package com.example.cmpt276project.ui.TakeBreath;

import androidx.appcompat.app.AppCompatActivity;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.cmpt276project.R;
import java.util.Objects;

/*  This class is responsible for the breath activity. Its behavior is based on a Finite State
    Machine which is implemented with the help of a matrix that holds the Runnable to be run for
    each transition.
 */
public class TakeBreathActivity extends AppCompatActivity {
    // Number of breaths
    int numberOfBreaths = 2;

    // Resources -------------------------------------------------------------------
    private Button setBreathButton;
    private ImageView takeBreathButtonGreen;
    private ImageView takeBreathButtonBlue;
    private TextView takeBreathButtonText;
    private EditText takeBreathEditText;
    private TextView takeBreathHelp;
    private TextView takeBreathDisplay;

    // Animations ------------------------------------------------------------------
    private AnimatorSet inhale;
    private AnimatorSet exhale;

    // Sounds ----------------------------------------------------------------------
    private MediaPlayer breathInPlayer;
    private MediaPlayer breathOutPlayer;

    // Finite State Machine ---------------------------------------------------------
    private enum State {
        NONE,
        BEGIN,
        WAITING_TO_INHALE,
        INHALING,
        INHALED_FOR_3S,
        INHALED_FOR_10S,
        EXHALE,
        EXHALE_3S,
        DONE
    }

    /*  2D array with executables that should be run for each respective
        state transition. To get the executable for state transition corresponding
        to {A, B}, do transitions[A][B] (where A and B are enum State ordinal values).
     */
    private Runnable[][] transitions;
    private Runnable[] pressActions;
    private Runnable[] releaseActions;

    private State fromState;        // For debugging purposes
    private State toState;          // For debugging purposes

    private State currentState;

    // Timers for state transition
    private Handler inhalingHandler3s;
    private Runnable inhalingAction3s;
    private Handler exhalingHandler3s;
    private Runnable exhalingAction3s;

    // Setup Methods ---------------------------------------------------------------
    public static Intent makeIntent(Context context) {
        return new Intent(context, TakeBreathActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_breath);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        getViews();
        setupListeners();
        initializeSound();
        initializeFSM();
    }

    private void initializeSound() {
        breathInPlayer = MediaPlayer.create(this, R.raw.breathin);
        breathOutPlayer = MediaPlayer.create(this, R.raw.breathout);
    }

    private void setupListeners() {
        setBreathButtonListener();
        setTakeBreathButtonListener();
    }

    private void setBreathButtonListener() {
        setBreathButton.setOnClickListener(v -> {
            if (!takeBreathEditText.getText().toString().equals(getString(R.string.message_empty))) {
                try {
                    int tempNumberOfBreaths = Integer.parseInt(takeBreathEditText.getText().toString());
                    if (tempNumberOfBreaths <= 0 || tempNumberOfBreaths > 10) {
                        Toast.makeText(
                                this,
                                R.string.message_error_out_of_range,
                                Toast.LENGTH_SHORT
                        ).show();
                    } else {
                        numberOfBreaths = tempNumberOfBreaths;
                    }
                } catch (NumberFormatException e) {
                    Toast.makeText(
                            this,
                            R.string.message_error_enter_int,
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
            changeButtonText(takeBreathDisplay, getString(R.string.num_breaths_remaining, numberOfBreaths));
        });
    }

    private void getViews() {
        takeBreathButtonGreen = (ImageView) findViewById(R.id.take_breath_button_green);
        takeBreathButtonBlue = (ImageView) findViewById(R.id.take_breath_button_blue);
        setBreathButton = (Button) findViewById(R.id.set_breaths);
        takeBreathEditText = (EditText) findViewById(R.id.take_breath_edit_text);
        takeBreathButtonText = (TextView) findViewById(R.id.take_breath_button_text);
        takeBreathHelp = (TextView) findViewById(R.id.take_breath_help);
        takeBreathDisplay = (TextView) findViewById(R.id.take_breath_display);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setTakeBreathButtonListener() {
        takeBreathButtonGreen.setOnTouchListener((v, event) -> {
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    pressActions[currentState.ordinal()].run();
                    break;

                case MotionEvent.ACTION_UP:
                    releaseActions[currentState.ordinal()].run();
                    break;
                default:
            }
            return true;
        });
    }

    // Finite State Machine Methods ------------------------------------------------
    // Changes the current state to a new state and perform the transition action
    private void stateTransition(State newState) {
        fromState = currentState;
        toState = newState;
        transitions[currentState.ordinal()][newState.ordinal()].run();
        currentState = newState;
    }

    // Initialize all aspects of the Finite State Machine
    private void initializeFSM() {
        currentState = State.NONE;
        initializeFSMTransitions();
        initializeFSMPressActions();
        initializeFSMReleaseActions();
        stateTransition(State.BEGIN);
    }

    // Actions to be taken when button is released while in a certain state
    private void initializeFSMReleaseActions() {
        int length = State.values().length;
        releaseActions = new Runnable[length];

        // Define release actions
        releaseActions[State.NONE.ordinal()] = () -> {
            throw new RuntimeException(getString(R.string.message_debug));
        };

        releaseActions[State.BEGIN.ordinal()] = () -> { /* do nothing */ };

        releaseActions[State.WAITING_TO_INHALE.ordinal()] = () -> { /* do nothing */ };

        releaseActions[State.INHALING.ordinal()] = () -> stateTransition(State.WAITING_TO_INHALE);

        releaseActions[State.INHALED_FOR_3S.ordinal()] = () -> stateTransition(State.EXHALE);

        releaseActions[State.INHALED_FOR_10S.ordinal()] = () -> stateTransition(State.EXHALE);

        releaseActions[State.EXHALE.ordinal()] = () -> { /* do nothing */ };

        releaseActions[State.EXHALE_3S.ordinal()] = () -> { /* do nothing */ };

        releaseActions[State.DONE.ordinal()] = () -> { /* do nothing */ };
    }

    // Actions to be taken when a button is pressed while in a certain state
    private void initializeFSMPressActions() {
        int length = State.values().length;
        pressActions = new Runnable[length];

        // Define release actions
        pressActions[State.NONE.ordinal()] = () -> {
            throw new RuntimeException(getString(R.string.message_debug));
        };

        pressActions[State.BEGIN.ordinal()] = () -> stateTransition(State.WAITING_TO_INHALE);

        pressActions[State.WAITING_TO_INHALE.ordinal()] = () -> stateTransition(State.INHALING);

        pressActions[State.INHALING.ordinal()] = () -> { /* do nothing */ };

        pressActions[State.INHALED_FOR_3S.ordinal()] = () -> { /* do nothing */ };

        pressActions[State.INHALED_FOR_10S.ordinal()] = () -> { /* do nothing */ };

        pressActions[State.EXHALE.ordinal()] = () -> { /* do nothing */ };

        pressActions[State.EXHALE_3S.ordinal()] = () -> {
            State newState;
            if (numberOfBreaths > 0) {
                newState = State.WAITING_TO_INHALE;
            } else {
                newState = State.DONE;
            }
            stateTransition(newState);
        };

        pressActions[State.DONE.ordinal()] = () -> { /* do nothing */ };
    }

    /*  In this method, all actions to be taken on transitions will be defined.
        Non-defined transitions are assigned a default action which will throw
        a RuntimeError for debugging convenience.
     */
    private void initializeFSMTransitions() {
        int length = State.values().length;
        transitions = new Runnable[length][length];

        // Default initialize all state transitions
        Runnable defaultTransition = () -> {
            throw new RuntimeException(getString(R.string.message_debug_alt, fromState, toState));
        };

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                transitions[i][j] = defaultTransition;
            }
        }

        // Define valid state transitions
        transitions[State.NONE.ordinal()][State.BEGIN.ordinal()] = () -> {
            takeBreathButtonText.setText(R.string.take_breath_button);
            takeBreathHelp.setText(R.string.help_begin_state);
            takeBreathDisplay.setText(getString(R.string.num_breaths_remaining, numberOfBreaths));
        };

        transitions[State.BEGIN.ordinal()][State.WAITING_TO_INHALE.ordinal()] = () -> {
            changeButtonText(takeBreathButtonText, getString(R.string.message_in));
            changeButtonText(takeBreathHelp, getString(R.string.message_breath_in));
            fadeOutChangeBreathNumber();
        };

        transitions[State.WAITING_TO_INHALE.ordinal()][State.INHALING.ordinal()] = () -> {
            beginInhaleAnimation();
            inhalingAction3s = () -> stateTransition(State.INHALED_FOR_3S);
            inhalingHandler3s = new Handler();
            inhalingHandler3s.postDelayed(inhalingAction3s, 3000);
            // Start inhale sound
        };

        transitions[State.INHALING.ordinal()][State.INHALED_FOR_3S.ordinal()] = () ->
                changeButtonText(takeBreathButtonText, getString(R.string.message_out));

        transitions[State.INHALING.ordinal()][State.WAITING_TO_INHALE.ordinal()] = () -> {
            cancelAnimationAndDefaultView(inhale);
            inhalingHandler3s.removeCallbacks(inhalingAction3s);
        };

        transitions[State.INHALED_FOR_3S.ordinal()][State.INHALED_FOR_10S.ordinal()] = () ->
                changeButtonText(takeBreathHelp, getString(R.string.message_breath_out_alt));

        transitions[State.INHALED_FOR_3S.ordinal()][State.EXHALE.ordinal()] = () -> {
            inhale.cancel();
            changeButtonText(takeBreathHelp, getString(R.string.message_breath_out));
            exhalingAction3s = () -> stateTransition(State.EXHALE_3S);
            exhalingHandler3s = new Handler();
            exhalingHandler3s.postDelayed(exhalingAction3s, 3000);
            beginExhaleAnimation();
        };

        transitions[State.INHALED_FOR_10S.ordinal()][State.EXHALE.ordinal()] = () -> {
            changeButtonText(takeBreathHelp, getString(R.string.message_breath_out));
            exhalingAction3s = () -> stateTransition(State.EXHALE_3S);
            exhalingHandler3s = new Handler();
            exhalingHandler3s.postDelayed(exhalingAction3s, 3000);
            beginExhaleAnimation();
        };

        transitions[State.EXHALE.ordinal()][State.EXHALE_3S.ordinal()] = () -> {
            numberOfBreaths--;
            String buttonMessage;
            String displayMessage;
            if (numberOfBreaths > 0) {
                buttonMessage = getString(R.string.message_in);
                displayMessage = getString(R.string.num_breaths_remaining, numberOfBreaths);
            } else {
                buttonMessage = getString(R.string.message_good_job);
                displayMessage = getString(R.string.message_empty);
                changeButtonText(takeBreathHelp, getString(R.string.message_empty));
            }
            changeButtonText(takeBreathButtonText, buttonMessage);
            changeButtonText(takeBreathEditText, buttonMessage);
            changeButtonText(takeBreathDisplay, displayMessage);
        };

        transitions[State.EXHALE_3S.ordinal()][State.DONE.ordinal()] = () ->
                cancelAnimationAndDefaultView(exhale);

        transitions[State.EXHALE_3S.ordinal()][State.WAITING_TO_INHALE.ordinal()] = () -> {
            cancelAnimationAndDefaultView(exhale);
            changeButtonText(takeBreathHelp, getString(R.string.message_breath_in));
        };
    }

    // Animation Methods -----------------------------------------------------------
    private void changeButtonText(TextView view, String string) {
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);

        fadeOut.setDuration(500);
        fadeIn.setDuration(500);

        fadeOut.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) { }
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setText(string);
                fadeIn.start();
            }
            @Override
            public void onAnimationCancel(Animator animation) { }
            @Override
            public void onAnimationRepeat(Animator animation) { }
        });
        fadeOut.start();
    }

    private void beginInhaleAnimation() {
        inhale = new AnimatorSet();
        ObjectAnimator expandXGreen = ObjectAnimator.ofFloat(
                takeBreathButtonGreen,
                "scaleX",
                1f,
                2f
        );
        ObjectAnimator expandYGreen = ObjectAnimator.ofFloat(
                takeBreathButtonGreen,
                "scaleY",
                1f,
                2f
        );
        ObjectAnimator greenInvisible = ObjectAnimator.ofFloat(
                takeBreathButtonGreen,
                "alpha",
                1f,
                0f
        );
        ObjectAnimator expandXBlue = ObjectAnimator.ofFloat(
                takeBreathButtonBlue,
                "scaleX",
                1f,
                2f
        );
        ObjectAnimator expandYBlue = ObjectAnimator.ofFloat(
                takeBreathButtonBlue,
                "scaleY",
                1f,
                2f
        );
        ObjectAnimator blueVisible = ObjectAnimator.ofFloat(
                takeBreathButtonBlue,
                "alpha",
                0f,
                1f
        );
        inhale.play(expandXGreen).
                with(expandYGreen).
                with(greenInvisible).
                with(expandXBlue).
                with(expandYBlue).
                with(blueVisible);
        inhale.setDuration(10000);
        inhale.addListener(new Animator.AnimatorListener() {
            private boolean canceled = false;

            @Override
            public void onAnimationStart(Animator animation) {
                breathInPlayer.start();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                breathInPlayer.stop();
                breathInPlayer.prepareAsync();
                if (!canceled) {
                    stateTransition(State.INHALED_FOR_10S);
                }
                canceled = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                canceled = true;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        inhale.start();
    }

    private void cancelAnimationAndDefaultView(Animator animator) {
        if (animator.isRunning()) {
            animator.cancel();
        }
        takeBreathButtonGreen.setScaleX(1f);
        takeBreathButtonGreen.setScaleY(1f);
        takeBreathButtonGreen.setAlpha(1f);

        takeBreathButtonBlue.setScaleX(1f);
        takeBreathButtonBlue.setScaleY(1f);
        takeBreathButtonBlue.setAlpha(0f);
    }

    private void beginExhaleAnimation() {
        exhale = new AnimatorSet();
        ObjectAnimator contractXGreen = ObjectAnimator.ofFloat(
                takeBreathButtonGreen,
                "scaleX",
                takeBreathButtonGreen.getScaleX(),
                1f
        );
        ObjectAnimator contractYGreen = ObjectAnimator.ofFloat(
                takeBreathButtonGreen,
                "scaleY",
                takeBreathButtonGreen.getScaleY(),
                1f
        );
        ObjectAnimator visibleGreen = ObjectAnimator.ofFloat(
                takeBreathButtonGreen,
                "alpha",
                takeBreathButtonGreen.getAlpha(),
                1f
        );

        ObjectAnimator contractXBlue = ObjectAnimator.ofFloat(
                takeBreathButtonBlue,
                "scaleX",
                takeBreathButtonBlue.getScaleX(),
                1f
        );
        ObjectAnimator contractYBlue = ObjectAnimator.ofFloat(
                takeBreathButtonBlue,
                "scaleY",
                takeBreathButtonBlue.getScaleY(),
                1f
        );
        ObjectAnimator invisibleBlue = ObjectAnimator.ofFloat(
                takeBreathButtonBlue,
                "alpha",
                takeBreathButtonBlue.getAlpha(),
                0f
        );
        exhale.play(contractXGreen).
                with(contractYGreen).
                with(visibleGreen).
                with(contractXBlue).
                with(contractYBlue).
                with(invisibleBlue);
        exhale.setDuration(10000);
        exhale.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                breathOutPlayer.start();
            }
            @Override
            public void onAnimationEnd(Animator animation) {
                breathOutPlayer.stop();
                breathOutPlayer.prepareAsync();
                State newState;
                if (numberOfBreaths > 0) {
                    newState = State.WAITING_TO_INHALE;
                } else {
                    newState = State.DONE;
                }
                stateTransition(newState);
            }
            @Override
            public void onAnimationCancel(Animator animation) {}
            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        exhale.start();
    }

    private void fadeOutChangeBreathNumber() {
        AnimatorSet fadeOut = new AnimatorSet();
        ObjectAnimator buttonFadeOut = ObjectAnimator.ofFloat(
                setBreathButton,
                "alpha",
                1f,
                0f
        );
        ObjectAnimator editTextFadeOut = ObjectAnimator.ofFloat(
                takeBreathEditText,
                "alpha",
                1f,
                0f
        );
        fadeOut.play(buttonFadeOut).with(editTextFadeOut);
        fadeOut.setDuration(500);
        fadeOut.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {}
            @Override
            public void onAnimationEnd(Animator animation) {
                setBreathButton.setVisibility(View.INVISIBLE);
                takeBreathEditText.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAnimationCancel(Animator animation) {}
            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        fadeOut.start();
    }
}