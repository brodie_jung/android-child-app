package com.example.cmpt276project.ui.CoinFlip;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import models.AppManager;
import models.Child;
import models.CoinFlip;

/*  Class responsible for displaying the history of coin flips.
    This class displays, in each entry, the coin flip, the corresponding date of the coin flip, the
    child who won the coin flip (if a child was selected), the date and time in which the coin flip
    was performed, and orders them in reverse chronological order.
 */
public class FlipHistoryActivity extends AppCompatActivity {
    private final AppManager manager = AppManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fliphistory_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        displayAll();
        buttonSetup();
    }

    public static Intent makeIntent(Context context) {
        return new Intent(context, FlipHistoryActivity.class);
    }

    private void buttonSetup() {
        Button buttonShowAll = findViewById(R.id.button_show_all);
        Button buttonChildOnly = findViewById(R.id.button_show_child_only);
        buttonShowAll.setOnClickListener(v -> displayAll());
        buttonChildOnly.setOnClickListener(v -> displayOneChild());
    }

    private void displayAll() {
        ListView viewCoinFlips = findViewById(R.id.list_coin_flips);
        ArrayList<CoinFlip> coinFlipArrayList = manager.getCoinFlipList();
        viewCoinFlips.setAdapter(new CustomAdaptor(coinFlipArrayList));
    }

    private void displayOneChild() {
        TextView viewEmpty = findViewById(R.id.text_empty_coin_flip);
        ListView listViewCoinFlip = findViewById(R.id.list_coin_flips);
        if (manager.numberOfCoinFlips() == 0) {
            viewEmpty.setVisibility(View.VISIBLE);
            listViewCoinFlip.setVisibility(View.INVISIBLE);
            viewEmpty.setText(getString(R.string.empty_child));
        } else {
            viewEmpty.setVisibility(View.INVISIBLE);
            listViewCoinFlip.setVisibility(View.VISIBLE);
            ArrayList<CoinFlip> coinFlipArrayList;
            try {
                coinFlipArrayList = manager.getChildCoinFlips(manager.getLastFlipped());
            } catch (Exception e) {
                coinFlipArrayList = manager.getChildCoinFlips();
            }
            listViewCoinFlip.setAdapter(new CustomAdaptor(coinFlipArrayList));
        }
    }

    private class CustomAdaptor extends BaseAdapter {
        private final List<CoinFlip> mFlips;
        public CustomAdaptor(List<CoinFlip> flips){
            this.mFlips = flips;
        }

        @Override
        public int getCount() {
            return mFlips.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            @SuppressLint({"ViewHolder", "InflateParams"})
            View view = getLayoutInflater().inflate(R.layout.custom_listview, null);
            ImageView currImageView = (ImageView)view.findViewById(R.id.child_image_view);
            TextView currTextView = (TextView)view.findViewById(R.id.child_text_view);
            setTextViewProperties(currTextView);
            displayProfilePicture(position, currImageView,mFlips);
            currTextView.setText(mFlips.get(position).print());
            return view;
        }

        private void setTextViewProperties(TextView currTextView) {
            currTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            currTextView.setTextSize(14);
        }
    }

    private void displayProfilePicture(int coinflipIndex, ImageView refImageView, List<CoinFlip> coinFlipList) {
        // Check if child already has a profile picture set
        Child child = manager.getChild(coinFlipList.get(coinflipIndex).getName());
        if (child != null && child.getPath() != null && !child.getPath().equals(getString(R.string.empty_path))) {
            // Display the custom set profile picture
            refImageView.setImageBitmap(setChildPhoto(Uri.fromFile(new File(child.getPath()))));
        } else {
            // Display default profile picture
            refImageView.setImageResource(R.drawable.default_user);
        }
    }

    private Bitmap setChildPhoto(Uri path) {
        try {
            InputStream inputStream = getContentResolver().openInputStream(path);
            return BitmapFactory.decodeStream(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}