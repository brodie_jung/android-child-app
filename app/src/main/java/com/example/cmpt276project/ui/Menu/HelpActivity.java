package com.example.cmpt276project.ui.Menu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import java.util.Objects;

/*
    Name:    HelpActivity.java
    Purpose: Implements the Help screen UI.
    .        Allows the user to see a brief summary
    .        of what each feature in the App does.
*/
public class HelpActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_activity);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    public static Intent makeIntent(Context context) {
        return new Intent(context, HelpActivity.class);
    }
}