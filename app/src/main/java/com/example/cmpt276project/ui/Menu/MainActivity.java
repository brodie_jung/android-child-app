package com.example.cmpt276project.ui.Menu;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import com.example.cmpt276project.ui.Children.ChildrenActivity;
import com.example.cmpt276project.ui.CoinFlip.FlipSelectionActivity;
import com.example.cmpt276project.ui.TakeBreath.TakeBreathActivity;
import com.example.cmpt276project.ui.Tasks.TaskListActivity;
import com.example.cmpt276project.ui.Timer.TimerActivity;

/*  This class is responsible for the main menu.
    In this class, the user should be able to easily access either the timer module of the app or
    the coin flip.
 */
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        setupButtons();
    }

    //On click listeners for all the main screen buttons
    private void setupButtons() {
        setupFlipButton();
        setupTimerButton();
        setupChildrenButton();
        setupHelpButton();
        setupTasksButton();
        setupTakeBreathButton();
    }

    private void setupFlipButton() {
        Button flipButton = findViewById(R.id.button_flip_activity);
        flipButton.setOnClickListener(v -> {
            Intent intent = FlipSelectionActivity.makeIntent(MainActivity.this);
            startActivity(intent);
        });
    }

    private void setupTimerButton() {
        Button timerButton = findViewById(R.id.button_timer_activity);
        timerButton.setOnClickListener(v -> {
            Intent intent = TimerActivity.makeIntent(MainActivity.this);
            startActivity(intent);
        });
    }

    private void setupChildrenButton() {
        Button childButton = findViewById(R.id.button_child_activity);
        childButton.setOnClickListener(v -> {
            Intent intent = ChildrenActivity.makeIntent(MainActivity.this);
            startActivity(intent);
        });
    }

    private void setupHelpButton() {
        Button helpButton = findViewById(R.id.help_activity);
        helpButton.setOnClickListener(v -> {
            Intent intent = HelpActivity.makeIntent(MainActivity.this);
            startActivity(intent);
        });
    }

    private void setupTasksButton() {
        Button tasksButton = findViewById(R.id.button_task_list_activity);
        tasksButton.setOnClickListener(v -> {
            Intent intent = TaskListActivity.makeIntent(MainActivity.this);
            startActivity(intent);
        });
    }

    private void setupTakeBreathButton() {
        Button takeBreath = findViewById(R.id.button_take_breath_activity);
        takeBreath.setOnClickListener(v -> {
            Intent intent = TakeBreathActivity.makeIntent(MainActivity.this);
            startActivity(intent);
        });
    }
}