package com.example.cmpt276project.ui.CoinFlip;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Objects;
import java.util.Random;
import models.AppManager;
import models.Child;
import models.CoinFlip;

/*  Class responsible for the coin flip.
    This class should enable the user to perform the coin flip, where an animation for the coin flip
    will be run and, at the end, the coin face will be decided and a text showing the winner will be
    displayed.
 */
public class FlipActivity extends AppCompatActivity {
    private String flipResult;
    private final AppManager manager = AppManager.getInstance();
    private boolean coinSoundPlayed = false;            // Indicates if the coin flip sound has already been played

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.flip_activity);
        setupButtons();
    }

    @Override
    public void onBackPressed() {
        saveToFile();
        finish();
    }

    @Override
    public void onPause() {
        saveToFile();
        super.onPause();
    }

    private void setupButtons() {

        final ImageView coin = findViewById(R.id.flipping);
        CoinFlipAnimation animation = new CoinFlipAnimation(coin);
        animation.addListener(() -> {
            displayResult();
            saveFlip();
            saveToFile();
            animation.disableAnimation();
        });
        coin.setOnClickListener((v) -> {
            playCoinFlipSound();
            animation.startAnimation();
        });
    }

    private void playCoinFlipSound() {
        MediaPlayer coinFlip = MediaPlayer.create(this, R.raw.coinflip);
        if (!coinSoundPlayed){
            coinSoundPlayed = true;
            coinFlip.start();
        } else {
            coinFlip.reset();
        }
    }

    private void displayResult() {
        TextView textResult = findViewById(R.id.text_coin_flip_result);
        ImageView coin = findViewById(R.id.flipping);
        Random random = new Random();
        boolean result = random.nextBoolean();
        Intent intent = getIntent();
        if (intent.getBooleanExtra("Skip", false)) {
            if (result) {
                textResult.setText(getString(R.string.coin_flip_heads));
                flipResult = getString(R.string.flip_result_heads);
                coin.setImageResource(R.drawable.coin_img_heads);
            }
            else {
                textResult.setText(getString(R.string.coin_flip_tails));
                flipResult = getString(R.string.flip_result_tails);
                coin.setImageResource(R.drawable.coin_img_tails);
            }
        } else {
            ImageView picture = findViewById(R.id.child_picture_coinflip_activity);
            displayProfilePicture(manager.getLastFlipped(), picture);
            if (result) {
                String msg = getString(
                        R.string.message_heads,
                        manager.getLastFlipped().getName(),
                        intent.getStringExtra("Selection")
                );
                textResult.setText(msg);
                flipResult = getString(R.string.flip_result_heads);
                coin.setImageResource(R.drawable.coin_img_heads);
            } else {
                String msg = getString(
                        R.string.message_tails,
                        manager.getLastFlipped().getName(),
                        intent.getStringExtra("Selection")
                );
                textResult.setText(msg);
                flipResult = getString(R.string.flip_result_tails);
                coin.setImageResource(R.drawable.coin_img_tails);
            }
        }
    }

    /*
        Name:    displayProfilePicture()
        Purpose: Displays the current configured
        .        profile picture of the child
    */
    private void displayProfilePicture(Child child, ImageView refImageView) {
        // Check if child already has a profile picture set
        if (!child.getPath().equals(getString(R.string.empty_path))) {
            // Display the custom set profile picture
            refImageView.setImageBitmap(setChildPhoto(Uri.fromFile(new File(child.getPath()))));
        } else {
            // Display default profile picture
            refImageView.setImageResource(R.drawable.default_user);
        }
    }

    /*
        Name:    setChildPhoto()
        Purpose: Creates a bitmap representation of a photo from
        .        a given file path.
    */
    private Bitmap setChildPhoto(Uri path) {
        try {
            InputStream inputStream = getContentResolver().openInputStream(path);
            return BitmapFactory.decodeStream(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveFlip() {
        long time = Calendar.getInstance().getTimeInMillis();
        if (getIntent().getBooleanExtra("Skip", false)) {
            manager.addCoinFlip(new CoinFlip("None", "None", flipResult, time));
        } else {
            String name = manager.getLastFlipped().getName();
            String choice = getIntent().getStringExtra("Selection");
            manager.addCoinFlip(new CoinFlip(name, choice, flipResult, time));
        }
    }

    private void saveToFile() {
        SharedPreferences myPref = getSharedPreferences("PREF_FLIP", MODE_PRIVATE);
        SharedPreferences.Editor editor = myPref.edit();
        Gson gson = new Gson();
        int size = manager.numberOfCoinFlips();
        for (int i = 0; i < size; i++) {
            String json = gson.toJson(manager.getCoinFlip(i));
            editor.putString("CoinFlip" + i, json);
        }
        String json = gson.toJson(manager.getLastFlipped());
        editor.putString("LastFlipped", json);
        editor.putInt("NumOfCoinFlip", size);
        editor.apply();
    }

    public static Intent makeIntent(Context context) {
        return new Intent(context, FlipActivity.class);
    }
}