package com.example.cmpt276project.ui.Timer;

// Import classes
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cmpt276project.R;
import java.util.Locale;
import java.util.Objects;

/*
    Name:    TimerActivity.java
    Purpose: Implements the Countdown TimerActivity.
    .        Allows the user to choose several durations and
    .        even custom durations. Also allows the user to
    .        get a notification when the timer has finished
    .        if the user is not in the TimerActivity.
*/
public class TimerActivity extends AppCompatActivity {
    // UI Widgets
    private TextView timerDisplay;                          // Reference to the countdown display
    private TextView timerSpeed;                            // Reference to the TextView that shows the timer speed
    private EditText setTimerDurationDisplay;               // Reference to the Edittext where user inputs a custom duration
    private Button pauseButton;                             // Reference to the Pause Button
    private Button startButton;                             // Reference to the Start Button
    private Button resetButton;                             // Reference to the Reset Button
    private Button[] durationButtons;                       // Reference to the 5 pre-made duration option
    private CountDownTimer timeoutTimer;                    // Reference to the countdown timer itself

    // Flags for state machine
    private boolean isTimerRunning;                         // Indicates if the timer is running
    private boolean stateSelectingDuration;                 // Indicates if the current state allows the user to choose a duration
    private boolean stateStarted;                           // Indicates if the start button has been clicked
    private boolean statePaused;                            // Indicates if the pause button has been clicked
    private boolean stateDone;                              // Indicates if the timer has ended
    private boolean userSelectsDurationFirst;               // Indicates if the user selected a duration option before selecting a speed

    // Counters for timer
    private long timeRemaining;                             // Stores how much time remains
    private long timeEnd;                                   // Stores how much time has passed since the user has left the TimerActivity
    private long setCountDownInterval;                      // Sets the speed of how fast the timer will count down
    private long timeTotal;                                 // Store the total time the user inputted
    private int percentRemaining;                           // Stores the percentage of the countdown left

    // Active Response Widgets
    private Vibrator timerVibrate;                          // Reference to the Vibrator instance
    private MediaPlayer timerBeep;                          // Reference to the MediaPlayer instance

    // Global
    private final static int INPUT_INVALID = -1;            // Constant variable to indicate an invalid input has been inputted

    ProgressBar MyProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timer_activity);

        // Initialize back button in action bar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        // Initialize notification
        createNotificationChannel();

        // Activate Sound/Vibrate
        activateResponse();

        // Setup Widgets
        setupWidgets();

        // Setup Progress Bar
        setupProgressBar();
    }

    private void setupProgressBar() {
        MyProgressBar = findViewById(R.id.progressBar3);
        MyProgressBar.setVisibility(View.VISIBLE);
        MyProgressBar.setProgress(1000);
        MyProgressBar.setMax(1000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.timer_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return fetchSpeed(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Fetch saved data
        retrieveDataFromSharedPref();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Check if the timer is still running
        if (stateStarted) {
            // If so, create a notification that will execute later
            setNotificationAlarm();
        }
        // Check if the timer can be snoozed
        snoozeTimer();

        // Save data
        saveDataIntoSharedPref();
        if (timeoutTimer != null) {
            timeoutTimer.cancel();
        }

        // Kill activity at onStop
        finish();
    }

    private void saveDataIntoSharedPref() {
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("minutesRemaining", timeRemaining);
        editor.putLong("totalTime", timeTotal);
        editor.putInt("percentRemaining", percentRemaining);
        editor.putBoolean("isTimerActive", isTimerRunning);
        editor.putBoolean("isStateSelectingDuration", stateSelectingDuration);
        editor.putBoolean("isStateStarted", stateStarted);
        editor.putBoolean("isStatePaused", statePaused);
        editor.putBoolean("isStateDone", stateDone);
        editor.putBoolean("isSelectionFirst", userSelectsDurationFirst);
        editor.putLong("minutesEnd", timeEnd);
        editor.putLong("setInterval", setCountDownInterval);
        editor.apply();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // When the "arrow" (back button) is clicked, destroy the instance of the TimerActivity
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    /*
        Name:    activateResponse()
        Purpose: Creates an instance of the
        .        vibrator and mediaplayer objects.
    */
    private void activateResponse() {
        timerVibrate = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        timerBeep = MediaPlayer.create(this, R.raw.happy_chord);
    }

    private void retrieveDataFromSharedPref() {
        // defValue means default value
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        timeRemaining = prefs.getLong("minutesRemaining", -1);
        percentRemaining = prefs.getInt("percentRemaining", 1000);
        timeTotal = prefs.getLong("totalTime", -1);
        isTimerRunning = prefs.getBoolean("isTimerActive", false);
        stateSelectingDuration = prefs.getBoolean("isStateSelectingDuration", true);
        stateStarted = prefs.getBoolean("isStateStarted", false);
        statePaused = prefs.getBoolean("isStatePaused", false);
        stateDone = prefs.getBoolean("isStateDone", false);
        userSelectsDurationFirst = prefs.getBoolean("isSelectionFirst", false);
        setCountDownInterval = prefs.getLong("setInterval", 1000);
        setupButtons();
        setupTimeoutTimerState(prefs);
    }

    /*
        Name:    createNotificationChannel()
        Purpose: Sets up the NotificationManager
        .        and the NotificationChannel which
        .        will be used to create a notification.
    */
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.notification_chanel_name);
            String description = getString(R.string.notification_chanel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("notifyTimer", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void setupWidgets() {
        timerDisplay = (TextView)findViewById(R.id.timer_text);
        timerSpeed = (TextView)findViewById(R.id.text_view_speed);
        setTimerDurationDisplay = (EditText)findViewById(R.id.edit_text_custom_duration);
    }

    private void setupButtons() {
        setupDurationButtons();
        setupSetTimerDurationDisplay();
        setupSetSpeedDisplay();
        setupPauseButton();
        setupStartButton();
        setupResetButton();
    }

    /*
        Name:    setupDurationButtons()
        Purpose: Creates a button for each of the
        .        duration options. (eg. 1 minute)
        .        Stores each button into an array
        .        of buttons for simplicity.
    */
    private void setupDurationButtons() {
        durationButtons = new Button[5];
        for (int i = 0; i < durationButtons.length; i++) {
            durationButtons[i] = implementDurationButtons(fetchButtonMinute(i), i);
        }
    }

    /*
        Name:    setupSetTimerDurationDisplay()
        Purpose: Allows the Edittext where user inputs a
        .        custom duration to automatically update
        .        the countdown display when the user inputs
        .        a number.
    */
    private void setupSetTimerDurationDisplay() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                validateTimerDuration();
            }
        };
        setTimerDurationDisplay.addTextChangedListener(textWatcher);
    }

    private void setupSetSpeedDisplay() {
        timerSpeed.setText(getString(
                R.string.countdown_speed,
                "" + (100000 / setCountDownInterval)
        ));
    }

    private void setupPauseButton() {
        pauseButton = (Button)findViewById(R.id.button_pause);
        pauseButton.setOnClickListener(v -> pauseTimer());
    }

    private void setupStartButton() {
        startButton = (Button)findViewById(R.id.button_start);
        startButton.setOnClickListener(v -> {
            // If the user enters 0, it will show an error message
            if (timeRemaining == INPUT_INVALID) {
                Toast.makeText(this, getString(R.string.invalid_input_timeout_timer), Toast.LENGTH_SHORT).show();
            } else {
                startTimer();
            }
        });
    }

    private void setupResetButton() {
        resetButton = (Button)findViewById(R.id.button_reset);
        resetButton.setOnClickListener(v -> resetTimer());
    }

    /*
        Name:    setupSetTimeoutTimerState()
        Purpose: Sets up the initial layout look of the timeout
        .        timer. If the timer was previously paused, the
        .        screen will initialize in the paused state. If
        .        the timer was previously counting down, the screen
        .        will initialize with the timer continuing to count
        .        down. If the timer was previously or currently
        .        finished, the screen will initialize with the timer
        .        ringing and vibrating. If the timer was previously
        .        in the default layout where the user chooses a
        .        duration, then the screen will initialize in its
        .        default layout.
    */
    private void setupTimeoutTimerState(SharedPreferences prefs) {
        if (isTimerRunning) {
            if (stateDone) {
                setStateDone();
            }
            if (stateStarted) {
                timeEnd = prefs.getLong("minutesEnd", 0);
                timeRemaining = timeEnd - System.currentTimeMillis();
                if (timeRemaining < 0) {
                    stateStarted = false;
                    timerComplete();
                } else {
                    startTimer();
                }
            }
            if (statePaused) {
                lockSetTimerDuration(false, 50, fetchSetTimerDurationHint(false));
                setVisibilityDurationButtons(false, 50);
                updateTimerDisplay(false);
                MyProgressBar.setProgress(percentRemaining);
                setStatePaused();
            }
        } else {
            MyProgressBar.setProgress(percentRemaining);
            stateSelectingDuration = true;
            stateStarted = false;
            statePaused = false;
            updateTimeoutTimerState();
        }
    }

    /*
        Name:    updateTimeoutTimerState()
        Purpose: Switches the states of the layout in the TimerActivity.
        .        stateSelectionDuration
        .           -> All buttons are enabled except pause button
        .        stateStarted
        .           -> All buttons are disabled except pause button
        .        statePaused
        .           -> All buttons are disabled except start (resume)
        .              and reset buttons
        .        stateDone
        .           -> All buttons are disabled except reset button
    */
    private void updateTimeoutTimerState() {
        if (stateSelectingDuration) {
            setVisibilityPauseButton(false, 50);
            setVisibilityStartButton(true, 255);
            setVisibilityDurationButtons(true, 255);
            lockSetTimerDuration(true, 255, fetchSetTimerDurationHint(true));
        }
        if (stateStarted) {
            setVisibilityDurationButtons(false, 50);
            lockSetTimerDuration(false, 50, fetchSetTimerDurationHint(false));
            setVisibilityPauseButton(true, 255);
            setVisibilityStartButton(false, 50);
            setVisibilityResetButton(false, 50);
            if (timeRemaining < 0) {
                timerComplete();
            }
        }
        if (statePaused) {
            setStatePaused();
        }
        if (stateDone) {
            setStateDone();
        }
    }

    /*
        Name:    implementDurationButtons()
        Purpose: Sets the onclickListener for each duration option.
    */
    private Button implementDurationButtons(long selectedDuration, int btnIndex) {
        Button currButton = fetchButtonID(btnIndex);
        currButton.setOnClickListener(v -> {
            setTimerDurationDisplay.setText(getString(R.string.edit_text_custom_duration_disabled));
            timeRemaining = selectedDuration;
            timeTotal = selectedDuration;
            userSelectsDurationFirst = true;
            updateTimerDisplay(true);
        });
        return currButton;
    }

    private void updateTimerDisplay(boolean FLAG_SELECTED) {
        if (FLAG_SELECTED) {
            timeRemaining = (long)(timeRemaining / (1000.0 / setCountDownInterval));
            timeTotal = (long)(timeTotal / (1000.0 / setCountDownInterval));
        }
        int minutes = (int) (timeRemaining / setCountDownInterval) / 60;
        int seconds = (int) (timeRemaining / setCountDownInterval) % 60;
        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        timerDisplay.setText(timeLeftFormatted);

    }

    /*
        Name:    validateTimerDuration()
        Purpose: Validates if the user input to the EditText is valid or not.
        .        Inputs can't be 0 or greater than 99.
    */
    private void validateTimerDuration() {
        if ((setTimerDurationDisplay.getText().toString().matches("")) ||                       // Checks if the input is empty
                (setTimerDurationDisplay.getText().toString().matches("-")) ||                  // Checks if the input contains only a dash
                ((setTimerDurationDisplay.getText().toString().charAt(0) == '-') &&                   // Checks if the input contains a dash and other numbers
                        (setTimerDurationDisplay.getText().toString().length() > 1)) ||
                (Integer.parseInt(setTimerDurationDisplay.getText().toString()) == 0) ||              // Checks if the input is equal to 0
                (Integer.parseInt(setTimerDurationDisplay.getText().toString()) >= 100)) {            // Checks if the input is greater than 100
            timeRemaining = INPUT_INVALID;
        } else {
            timeRemaining = (long)(60000 * Integer.parseInt(setTimerDurationDisplay.getText().toString()) / (1000.0 / setCountDownInterval));
            timeTotal = (long)(60000 * Integer.parseInt(setTimerDurationDisplay.getText().toString()) / (1000.0 / setCountDownInterval));
            userSelectsDurationFirst = true;
        }
        updateTimerDisplay(false);
    }

    private Button fetchButtonID(int btnIndex) {
        Button currButton;
        switch (btnIndex)
        {
            case 0:
                currButton = (Button)findViewById(R.id.button_1_min);
                break;
            case 1:
                currButton = (Button)findViewById(R.id.button_2_min);
                break;
            case 2:
                currButton = (Button)findViewById(R.id.button_3_min);
                break;
            case 3:
                currButton = (Button)findViewById(R.id.button_5_min);
                break;
            case 4:
                currButton = (Button)findViewById(R.id.button_10_min);
                break;
            default:
                currButton = null;
        }
        return currButton;
    }

    private long fetchButtonMinute(int btnIndex) {
        long milliRemaining;
        switch (btnIndex)
        {
            case 0:
                milliRemaining = 60000;
                break;
            case 1:
                milliRemaining = 120000;
                break;
            case 2:
                milliRemaining = 180000;
                break;
            case 3:
                milliRemaining = 300000;
                break;
            case 4:
                milliRemaining = 600000;
                break;
            default:
                return -1;
        }
        return milliRemaining;
    }

    @SuppressLint("NonConstantResourceId")
    private boolean fetchSpeed(MenuItem item) {
        if (isTimerRunning || userSelectsDurationFirst) {
            Toast.makeText(this, getString(R.string.message_time_set), Toast.LENGTH_SHORT).show();
            return true;
        }
        switch (item.getItemId()) {
            case R.id.percent25:
                setCountDownInterval = 4000;
                break;
            case R.id.percent50:
                setCountDownInterval = 2000;
                break;
            case R.id.percent75:
                setCountDownInterval = 1333;
                break;
            case R.id.percent100:
                setCountDownInterval = 1000;
                break;
            case R.id.percent200:
                setCountDownInterval = 500;
                break;
            case R.id.percent300:
                setCountDownInterval = 333;
                break;
            case R.id.percent400:
                setCountDownInterval = 250;
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        setupSetSpeedDisplay();
        return true;
    }

    private String fetchSetTimerDurationHint(boolean isDefaultHint) {
        return isDefaultHint ? getString(R.string.edit_text_custom_duration) : getString(R.string.edit_text_custom_duration_disabled);
    }

    private void pauseTimer() {
        stateStarted = false;
        statePaused = true;
        updateTimeoutTimerState();
        timeoutTimer.cancel();
    }

    private void startTimer() {
        stateSelectingDuration = false;
        stateStarted = true;
        statePaused = false;
        updateTimeoutTimerState();
        timeEnd = System.currentTimeMillis() + timeRemaining;
        timeoutTimer = new CountDownTimer(timeRemaining, 50) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeRemaining = millisUntilFinished;
                percentRemaining =(int) (0.5d + ((double)timeRemaining/(double)timeTotal) * 1000);
                updateTimerDisplay(false);
                MyProgressBar.setProgress(percentRemaining,true);
            }
            @Override
            public void onFinish() {
                isTimerRunning = false;
                timerComplete();
            }
        }.start();
        isTimerRunning = true;
    }

    private void resetTimer() {
        if (stateDone) {
            snoozeTimer();
        }
        setCountDownInterval = 1000;
        timeRemaining = INPUT_INVALID;
        isTimerRunning = false;
        stateSelectingDuration = true;
        stateStarted = false;
        statePaused = false;
        stateDone = false;
        userSelectsDurationFirst = false;
        startButton.setText(getString(R.string.button_start));
        setTimerDurationDisplay.setText(getString(R.string.edit_text_custom_duration_disabled));
        MyProgressBar.setProgress(1000);
        percentRemaining = 1000;
        setupSetSpeedDisplay();
        updateTimeoutTimerState();
        updateTimerDisplay(false);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
        All of the following methods will either enable or disable a specified button.
        Methods are used in the different states of the layout, namely
            stateSelectedDuration, stateStarted, statePaused, stateDone
     */

    private void setVisibilityPauseButton(boolean isEnabled, int setAlpha) {
        pauseButton.setEnabled(isEnabled);
        pauseButton.getBackground().setAlpha(setAlpha);
    }

    private void setVisibilityStartButton(boolean isEnabled, int setAlpha) {
        startButton.setEnabled(isEnabled);
        startButton.getBackground().setAlpha(setAlpha);
    }

    private void setVisibilityResetButton(boolean isEnabled, int setAlpha) {
        resetButton.setEnabled(isEnabled);
        resetButton.getBackground().setAlpha(setAlpha);
    }

    private void setVisibilityDurationButtons(boolean isEnabled, int setAlpha) {
        for (Button durationButton : durationButtons) {
            durationButton.setEnabled(isEnabled);
            durationButton.getBackground().setAlpha(setAlpha);
        }
    }

    private void lockSetTimerDuration(boolean isEnabled, int setAlpha, String fetchSetTimerDurationHint) {
        if (isEnabled) {
            setTimerDurationDisplay.setFocusableInTouchMode(isEnabled);
        } else {
            setTimerDurationDisplay.setFocusable(isEnabled);
        }
        setTimerDurationDisplay.getBackground().setAlpha(setAlpha);
        setTimerDurationDisplay.setHint(fetchSetTimerDurationHint);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
        Name:    timerComplete()
        Purpose: Makes the timer ring and vibrate
        .        upon the timer finishing counting
        .        down.
    */
    private void timerComplete() {
        // Reset fields
        timeRemaining = 0;
        updateTimerDisplay(false);
        stateStarted = false;
        stateDone = true;
        MyProgressBar.setProgress(0);
        updateTimeoutTimerState();

        // Sound and Vibrate will loop until user performs an action
        timerBeep.setLooping(true);
        timerBeep.start();
        long[] vibePattern = { 0, 200, 0 };
        timerVibrate.vibrate(vibePattern, 0);
    }

    private void setStatePaused() {
        setVisibilityPauseButton(false, 50);
        setVisibilityStartButton(true, 255);
        startButton.setText(getString(R.string.button_start_paused));
        setVisibilityResetButton(true, 255);
    }

    private void setStateDone() {
        setVisibilityDurationButtons(false, 50);
        lockSetTimerDuration(false, 50, fetchSetTimerDurationHint(false));
        setVisibilityPauseButton(false, 50);
        setVisibilityStartButton(false, 50);
        setVisibilityResetButton(true, 255);
    }

    private void snoozeTimer() {
        if (stateDone) {
            timerBeep.setLooping(false);
            timerVibrate.cancel();
        }
    }

    /*
        Name:    setNotificationAlarm()
        Purpose: Schedules a notification to be created
        .        after a certain amount of time.
    */
    private void setNotificationAlarm() {
        long currTime = System.currentTimeMillis();
        Intent notiIntent = new Intent(TimerActivity.this, ReminderBroadcast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(TimerActivity.this, 0, notiIntent, 0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.set(
                AlarmManager.RTC_WAKEUP,
                currTime + timeRemaining - 1000,
                pendingIntent
        );
    }

    public static Intent makeIntent(Context context) {
        return new Intent(context, TimerActivity.class);
    }
}